# .bash_profile

# Get the aliases and functions
[ -f $HOME/.bashrc ] && . $HOME/.bashrc

export PATH=$PATH:$HOME/.local/bin/

export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share

export HISTFILE=$HOME/.local/share/bash/history
export GTK2_RC_FILES=$HOME/.config/gtk-2.0/gtkrc
export INPUTRC=$HOME/.config/readline/inputrc
