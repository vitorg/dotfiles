# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

shopt -s autocd
shopt -s histappend

alias xbpi="sudo xbps-install"
alias xbpr="sudo xbps-remove"
alias xbpq="sudo xbps-query"

alias ls="exa --group-directories-first -la"
alias la="exa -a --group-directories-first"
alias l="exa --group-directories-first"

alias smci="sudo make clean install"

alias config="/usr/bin/git --git-dir=$HOME/docs/dotfiles.git/ --work-tree=$HOME"
alias www-up="rsync -rtvzP /home/vitorg/docs/projects/website/ vitorg@tilde.team:/home/vitorg/public_html"

PS1='\[\e[0;1;91m\][\[\e[0;93m\]\u\[\e[0;94m\]@\[\e[0;92m\]\h \[\e[0;95m\]\W\[\e[0;1;91m\]] \[\e[0;96m\]$(git branch 2>/dev/null | grep '"'"'^*'"'"' | colrm 1 2)\n\[\e[0;1;37m\]$ \[\e[0m\]\[\e[0m\]'
alias bvclip="xclip -sel clipb ~/docs/boas-vindas.txt"
