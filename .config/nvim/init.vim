"
" ~/.config/nvim/init.vim
"

" Use clipboard instead of primary selection
set clipboard+=unnamedplus

" Vim-Plug
call plug#begin('~/.config/nvim/plugged')
Plug 'jacoborus/tender.vim'
Plug 'mattn/emmet-vim'
call plug#end()

" True Color Support
if (has("termguicolors"))
	set termguicolors
endif

" Dark background for dark theme
set background=dark

" Remove trailing spaces
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e
autocmd BufWritePre *.[ch] %s/\%$/\r/e

" Syntax highlighing, setting theme, cursorline and numbers
set cursorline relativenumber numberwidth=2
syntax enable

colorscheme tender

" No swapfile and backup
set noswapfile nobackup

" Emmet
let g:user_emmet_leader_key=','
let g:user_emmet_mode='n'

" Shortcut split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
